---
author: Samuel Salamon
title: Rok 2022
subtitle: Podsumowanie działań 6 Suwalskiego Szczepu Harcerskiego "Zorza" im. Alfreda Lityńskiego
date: Listopad 2023
theme: Berlin
output: beamer_presentation
header-includes: 
    \usepackage{xcolor}
    \usepackage{listings}
---

## Slajd 2 - Wyjazdy
1. Zimowisko "W 5 dni dookoła Świata"
2. Nocka Zuchowa "Śladami św. Franciszka"
3. Nocka integracyjna Elementals
4. Biwak Elementals "Chodź pomaluj mój Świat"
5. Obóz Wędrowny "Vaiana i Skarb Ocenau"
6. Biwak Jesienny "Na harcerzu polegaj jak na Zawiszy"

## Slajd 3 - Zimowisko "W 5 dni dookoła Świata"
\begin{block}{Zimowisko}
Czas trwania:  5 dni\\
Ilość Uczestników: około 60\\
Drużyny: Przyjaciele, Polaris, Elementals oraz jako współpracujące środowisko "To tajemnica"\\
Cel programowy: Poznanie różnych krain geograficznych i kultur\\
\end{block}

## Slajd 4 - Nocka Zuchowa "Śladami św. Franciszka"
\begin{block}{Nocka Zuchowa}
Czas trwania:  2 dni\\
Ilość Uczestników: około 20\\
Cel programowy: Poznanie postaci Św. Franciszka i jego podejścia do przyrody\\
\end{block}

## Slajd 5 - Biwak Elementals "Chodź pomaluj mój Świat"
\begin{block}{Biwak Elementals}
Czas trwania:  3 dni\\
Ilość Uczestników: około 10\\
Cel programowy: Odkrycie swojej artystycznej i emocjonalnej strony\\
\end{block}

## Slajd 6 - Obóz wędrowny "Vaiana i Skarb Oceanu"
\begin{block}{Obóz wędrowny}
Czas trwania:  13 dni\\
Ilość Uczestników: około 35\\
Drużyny: Przyjaciele, Polaris, Elementals\\
Cel programowy: Nauka wędrowania oraz życia w zgodzie z naturą\\
\end{block}

## Slajd 7 - Biwak Jesienny "Na harcerzu polegaj jak na Zawiszy"
\begin{block}{Biwak Jesienny}
Czas trwania:  3 dni\\
Ilość Uczestników: około 50\\
Drużyny: Przyjaciele, Polaris, Elementals\\
Cel programowy: Zapoznanie z historią Zawiszy Czarnego i najważniejszych faktów związanych z Grunwaldem\\
\end{block}

## Slajd 8 - Inne wydarzenia
### Część I
* Spotkania integracyjne rady szczepu w tym: sauna, mecz...
* Kurs HARP, który ukończyły Kasia oraz Ania
* Hufcowe DMB
* Szkolenie z nowego SIM
* Rozpoczęcie Roku harcerskiego
* Cokwartalne Msze św.
* Zebrania z rodzicami
* Pomoc na rzecz hufcowej bazy harcerskiej "Garbaś"
* Sejmik Grunwaldzki
* Obchody 3 maja, 11 listopada, 1 września, 
* Warty 1 listopada oraz sprzątanie grobów

## Slajd 9 - Inne wydarzenia
### Część II
* Kurs KKK, który ukończył Samuel
* Kurs Przewodnikowski, który ukończyły Ola, Wiktoria oraz Wiktoria
* Pomoc UKRAINIE
* Rajd Harcerski "Polariada"
* Zlot hufca
* Biwak Mikołajkowy
* Dzień Rycerzyka Niepokalanej
* Dzień Dziecka
* Dzień Ziemniaka
* Akcja Naborowa
* Akcja Zarobkowa
* WOŚP

## Slajd 10 - Praca jednostek - cz.1/3
### Przyjaciele
\begin{center}
\textbf{\textcolor{red}{Zdobywaliśmy odznakę srebrnego słoneczka}}
\end{center}

![Logo drużyny Przyjaciele](Przyjaciele.png){height=25% width=25%}

_Nie tylko zdobyliśmy tę odznakę jako gromada ale również od września zdobywamy jej wyższy złoty poziom. Uczestniczyliśmy w wielu organizowanych wydarzeniach oraz zdobyliśmy kilkanaście tropów i sprawności indywidualnych_

## Slajd 11 - Praca jednostek - cz.2/3
### Polaris
\begin{center}
\textbf{\textcolor{green}{Skupiliśmy się na tropach}}
\end{center}

![Logo drużyny Polaris](Polaris.png){height=25% width=25%}

_Eksperymentatorzy - nauczyliśmy się wykonywać wiele eksperymentów, które pomogły nam zrozumieć jak działa nasz świat
Poszukiwacze historii - 
odkrywamy historię Grunwaldu oraz naszych rodzimych terenów Suwalszczyzny_

## Slajd 12 - Praca jednostek - cz.3/3
### Elementals
\begin{center}
\textbf{\textcolor{blue}{Rozwijaliśmy swoje pasje}}
\end{center}

![Logo drużyny Elementals](Elementals.png){height=30% width=30%}

_Skoncentrowaliśmy się na rozwijaniu tego co nas interesuje: spotkanie z podróżnikiem, wyjście do movement arena na lasery, robienie własnego sushi, spotkanie z innym środowiskiem. Jednak to nie zatrzymało naszej służby na rzecz bazy, WOŚP oraz Ukrainy_